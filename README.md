# React Todo List

### This project is a copy of [Vue3 Todo List](https://gitlab.com/pPitohu/vue3-todo-list) rewritten onto react ecosystem (a bit). This is a test task to pass futher interview steps.

## Setup

Main libs I needed for the project:

- [Mantine UI](https://mantine.dev/) for UI and components.
- [Redux Toolkit](https://redux-toolkit.js.org/) for store management, nothing too fancy. [Redux-persist](https://github.com/rt2zz/redux-persist) for an auto-save to localstorage and restore from it.
- [dndkit](https://dndkit.com/) for DnD library, to move tasks back and forth.

Typescript here is used to handle types and not to forget something. I suppose this is not strongly typed app, but at least types exist.

Eslint is used to keep this app in one style and handle some errors. You can check custom config in `.eslintrc.cjs`. There is some additional (maybe experimental) eslint plugins to sort imports/props on a tag, remove unused imports.

Added ci/cd file to deploy to pages and "build on push" so I need only push/merge into main branch and it'll be built and deployed to pages automatically.

### Time spent: 6 hours of rewriting in total