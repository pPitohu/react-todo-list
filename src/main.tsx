import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import '@mantine/core/styles.css'
import '@/assets/scss/index.scss'
import 'react-toastify/dist/ReactToastify.css'
import classes from './main.module.css'
import { ToastContainer } from 'react-toastify'
import { PersistGate } from 'redux-persist/integration/react'
import { persistor, store } from './store'
import { Provider } from 'react-redux'
import { Input, MantineProvider, createTheme } from '@mantine/core'

const theme = createTheme({
  cursorType: 'pointer',
  components: {
    Input: Input.extend({ classNames: classes })
  }
})

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <MantineProvider theme={theme}>
          <App />
          <ToastContainer />
        </MantineProvider>
      </PersistGate>
    </Provider>
  </React.StrictMode>,
)
