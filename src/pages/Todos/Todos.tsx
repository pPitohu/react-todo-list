import checkmarkDoneIcon from '@/assets/icons/checkmark-done.svg'
import checklistIcon from '@/assets/icons/checklist.svg'
import TodosInput from '@/components/Todos/TodosInput/TodosInput'
import TodosList from '@/components/Todos/TodosList/TodosList'
import TodosFilters from '@/components/Todos/TodosFilters/TodosFilters'
import TodosStatusInfo from '@/components/Todos/TodosStatusInfo/TodosStatusInfo'
import './Todos.scss'
import { useAppSelector } from '@/store'

export default function Todos() {
  const tasks = useAppSelector(state => state.tasks)

  return (
    <div className="todos">
      <img
        className="todos__icon"
        src={checklistIcon}
        alt="checklist"
      />
      <p className="todos__title">
        Today I need to
      </p>
      <TodosInput />
      <TodosList />
      <TodosStatusInfo />
      {(tasks.length === 0) ?
        <div className="todos__helper-text">
          <img
            className="todos__helper-text__icon"
            src={checkmarkDoneIcon}
            alt="checkmark"
          />
          <span>Congrats, you have no more tasks to do</span>
        </div>
        : <TodosFilters />
      }
    </div>
  )
}
