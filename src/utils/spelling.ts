export const getNoun = (number: number, one: string, more: string) => {
  let n = Math.abs(number)
  n %= 100
  if (n >= 5 && n <= 20) {
    return more
  }
  n %= 10
  if (n === 1) {
    return one
  }
  return more
}
