import { MAX_NAME_LENGTH } from '@/store/todos/constants'
import { Task } from '@/store/todos/types'

export const isTaskNameValid = (taskName: string, tasks?: Task[]) => {
  if (!taskName)
    return {
      isValid: false,
      error: 'Name cannot be empty'
    }
  if (tasks && tasks.find(t => t.content === taskName))
    return {
      isValid: false,
      error: 'Name is already existing'
    }
  if (taskName.length > MAX_NAME_LENGTH)
    return {
      isValid: false,
      error: `Name should be ${MAX_NAME_LENGTH} characters max`
    }

  return { isValid: true, error: null }
}
