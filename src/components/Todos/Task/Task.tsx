import { useState } from 'react'
import type { Task } from '@/store/todos/types'
import dragHandleIcon from '@/assets/icons/drag-handle.svg'
import binIcon from '@/assets/icons/bin.svg'
import doneIcon from '@/assets/icons/done.svg'
import pencilIcon from '@/assets/icons/pencil.svg'
import { Checkbox, Input } from '@mantine/core'
import { renameTask, removeTask, toggleTaskCompleted } from '@/store/todos'
import { useAppDispatch } from '@/store'
import { useSortable } from '@dnd-kit/sortable'
import { UniqueIdentifier } from '@dnd-kit/core'
import { CSS } from '@dnd-kit/utilities'
import './Task.scss'
import { isTaskNameValid } from '@/utils/validation'
import { toast } from 'react-toastify'
import clsx from 'clsx'

interface TaskProps {
  tasks: Task[],
  item: Task,
  id: UniqueIdentifier,
  shouldHideDragHandle: boolean,
  toggleDragHandles: () => void
}

export default function Task({
  tasks,
  item,
  id,
  shouldHideDragHandle,
  toggleDragHandles
}: TaskProps) {

  const [ newTaskContent, setNewTaskContent ] = useState(item.content)
  const [ isEditing, setIsEditing ] = useState(false)
  
  const dispatch = useAppDispatch()
  const {
    attributes,
    listeners,
    setNodeRef,
    transform,
    transition,
  } = useSortable({ id })

  const style = {
    transform: CSS.Transform.toString(transform),
    transition,
  }

  const toggleIsEditing = () => {
    toggleDragHandles()
    setIsEditing(!isEditing)
  }
  const toggleTaskCompletion = () => dispatch(toggleTaskCompleted(item.id))
  const removeTodoTask = () => dispatch(removeTask(item.id))
  const saveNewContent = () => {
    if (newTaskContent.trim() === item.content) return toggleIsEditing()
    const { isValid, error } = isTaskNameValid(newTaskContent, tasks)
    if (!isValid)
      return toast(error, { type: 'error' })
    dispatch(renameTask({ id: item.id, newTaskContent }))
    toggleIsEditing()
  }

  return (
    <li
      ref={setNodeRef}
      style={style}
      className={clsx('task', item.isCompleted && 'completed')}
    >
      <img
        id="drag-handle"
        className={clsx('task__drag-handle', shouldHideDragHandle && 'editing')}
        src={dragHandleIcon}
        alt="drag-handle"
        {...attributes}
        {...listeners}
      />
      <div className="task__content" onClick={!isEditing ? toggleTaskCompletion : () => {}}>
        {isEditing ? 
          <Input 
            size="xxs"
            radius="md"
            value={newTaskContent}
            onChange={e => setNewTaskContent(e.target.value)}
            className="task__content-input"
            placeholder="New name"
          />
          :
          <Checkbox
            checked={item.isCompleted}
            onChange={() => { }}
            className="task__content-checkbox"
            label={<span className="strike">{item.content}</span>}
          />
        }
      </div>
      <div className="task__actions">
        {isEditing ?
          <img
            src={doneIcon}
            alt="save"
            onClick={saveNewContent}
          />
          :
          <img
            className="task__actions-edit"
            src={pencilIcon}
            alt="pencil"
            onClick={toggleIsEditing}
          />
        }
        <img
          className="task__actions-remove"
          src={binIcon}
          alt="bin"
          onClick={removeTodoTask}
        />
      </div>
    </li>
  )
}
