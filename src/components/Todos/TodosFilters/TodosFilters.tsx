import { useAppDispatch, useAppSelector } from '@/store'
import { Filter } from '@/store/todos/types'
import { clearCompletedTasks, completeAllTasks, completedTasksSelector, filterTasks } from '@/store/todos'
import { Button } from '@mantine/core'
import { useAutoAnimate } from '@formkit/auto-animate/react'
import { useMemo } from 'react'
import './TodosFilters.scss'

export default function TodosFilters() {

  const tasks = useAppSelector(state => state.tasks)
  const completedTasks = useAppSelector(completedTasksSelector)
  const filter = useAppSelector(state => state.filter)
  const isEverythingCompleted = tasks.length === completedTasks.length

  const dispatch = useAppDispatch()
  const [parent] = useAutoAnimate()

  const completeEverything = () => dispatch(completeAllTasks())
  const changeFilter = (id: Filter) => dispatch(filterTasks(id))
  const clearCompleted = () => dispatch(clearCompletedTasks())

  const filterTabs = useMemo(() => [
    {
      title: 'All',
      id: Filter.all,
      isShown: true,
      buttonVariant: filter === Filter.all ? 'filled' : 'outline'
    },
    {
      title: 'Not completed',
      id: Filter.notCompleted,
      isShown: !isEverythingCompleted,
      buttonVariant: filter === Filter.notCompleted ? 'filled' : 'outline'
    },
    {
      title: 'Completed',
      id: Filter.completed,
      isShown: !isEverythingCompleted && completedTasks.length > 0,
      buttonVariant: filter === Filter.completed ? 'filled' : 'outline'
    }
  ], [ completedTasks.length, filter ])

  return (
    <div className="todos__filters" ref={parent}>
      {!isEverythingCompleted &&
        <Button
          radius="md"
          size="xs"
          variant="default"
          className="todos__filters-button"
          onClick={completeEverything}
        >
            Check all
        </Button>
      }
      <div className="todos__filters-tabs">
        <Button.Group className="todos__filters-tabs__group" ref={parent}>
          {filterTabs.map(({ title, id, isShown, buttonVariant }) => {
            return isShown && 
              <Button
                radius="md"
                size="xs"
                variant={buttonVariant}
                className="todos__filters-tabs__tab"
                onClick={() => changeFilter(id)}
                key={title}
              >
                { title }
              </Button>
          })
          }
        </Button.Group>
      </div>  
      { isEverythingCompleted &&
        <Button
          radius="md"
          variant="default"
          size="xs"
          className="todos__filters-button"
          onClick={clearCompleted}
        >
            Clear completed
        </Button>
      }
    </div>
  )
}
