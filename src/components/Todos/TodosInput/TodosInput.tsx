import { addNewTask } from '@/store/todos'
import { FormEvent, useState } from 'react'
import { useAppDispatch, useAppSelector } from '@/store'
import './TodosInput.scss'
import { Button, Input } from '@mantine/core'
import { isTaskNameValid } from '@/utils/validation'
import { useAutoAnimate } from '@formkit/auto-animate/react'

export default function TodosInput() {
  const [ taskName, setTaskName ] = useState('')
  const [ error, setError ] = useState<string | null>('')
  const [parent] = useAutoAnimate()

  const tasks = useAppSelector(state => state.tasks)
  const dispatch = useAppDispatch()
  const addTask = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    setError('')
    const { isValid, error } = isTaskNameValid(taskName, tasks)
    if (!isValid)
      return setError(error)

    dispatch(addNewTask(taskName))
    setTaskName('')
  }
  return (
    <form
      className="todos-input"
      onSubmit={addTask}
      ref={parent}
    >
      <Input.Wrapper
        className="todos-input__field"
        error={error}>
        <Input
          radius="md"
          value={taskName}
          onChange={e => setTaskName(e.target.value)}
          placeholder="Add new todo..."
        />
      </Input.Wrapper>
      
      {(taskName || tasks.length > 0) &&
        <Button
          className="todos-input__button"
          radius="md"
          type="submit"
          color="indigo"
        >
          Submit
        </Button>
      }
    </form>
  )
}
