import { useAppDispatch, useAppSelector } from '@/store'
import Task from '@/components/Todos/Task/Task.tsx'
import { DndContext, DragEndEvent, PointerSensor, closestCenter, useSensor, useSensors } from '@dnd-kit/core'
import './TodosList.scss'
import { SortableContext, arrayMove, verticalListSortingStrategy } from '@dnd-kit/sortable'
import { setTasks } from '@/store/todos'
import { restrictToFirstScrollableAncestor, restrictToVerticalAxis } from '@dnd-kit/modifiers'
import { useState } from 'react'

export default function TodosList() {

  const [ shouldHideDragHandles, setShouldHideDragHandles ] = useState(false)
  const tasks = useAppSelector(state => state.tasks)
  const dispatch = useAppDispatch()

  const sensors = useSensors(
    useSensor(PointerSensor)
  )

  const reorderTasks = (event: DragEndEvent) => {
    const { active, over } = event

    if (active.id === over?.id) return
    const oldIndex = tasks.findIndex(t => t.id === active.id)
    const newIndex = tasks.findIndex(t => t.id === over!.id)
    
    const reorderedTasks = arrayMove(tasks, oldIndex, newIndex)
    dispatch(setTasks(reorderedTasks))
  }
  
  const toggleDragHandles = () => setShouldHideDragHandles(!shouldHideDragHandles)

  if (tasks.length === 0) return null
  return (
    <div className="todos__list">
      <DndContext
        sensors={sensors}
        collisionDetection={closestCenter}
        onDragEnd={reorderTasks}
        modifiers={[ restrictToFirstScrollableAncestor, restrictToVerticalAxis ]}
      >
        <ul>
          <SortableContext
            items={tasks}
            strategy={verticalListSortingStrategy}
          >
            {tasks.map(task => {
              return !task.isHidden &&
                <Task
                  tasks={tasks}
                  id={task.id}
                  item={task}
                  key={task.id}
                  shouldHideDragHandle={shouldHideDragHandles}
                  toggleDragHandles={toggleDragHandles}
                />
            })}
          </SortableContext>
        </ul>
      </DndContext>
    </div>
  )
}
