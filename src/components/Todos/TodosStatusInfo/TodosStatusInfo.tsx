import { Progress } from '@mantine/core'
import { useAppSelector } from '@/store'
import { completedTasksSelector, tasksToBeFinishedSelector } from '@/store/todos'
import { magentaColor, pinkColor } from '@/utils/colors'
import './TodosStatusInfo.scss'
import { getNoun } from '@/utils/spelling'

export default function TodosStatusInfo() {
  const tasks = useAppSelector(state => state.tasks)
  const completedTasks = useAppSelector(completedTasksSelector)
  const tasksToBeFinished = useAppSelector(tasksToBeFinishedSelector)

  const completedTasksPercent = (completedTasks.length / tasks.length) * 100
  const tasksToBeFinishedPercent = (tasksToBeFinished.length / tasks.length) * 100

  if (tasks.length === 0) return null
  return (
    <div className="todos__status-info">
      <div className="info-block">
        <p className="info-block__data">
          { completedTasks.length } { getNoun(completedTasks.length, 'task', 'tasks') }
        </p>
        <h2 className="info-block__title">
          Completed
        </h2>
        <Progress
          className="info-block__progress-bar"
          transitionDuration={300}
          value={completedTasksPercent}
          color={magentaColor}
        />
      </div>
      <div className="info-block">
        <p className="info-block__data">
          { tasksToBeFinished.length } { getNoun(tasksToBeFinished.length, 'task', 'tasks') }
        </p>
        <h2 className="info-block__title">
          To be finished
        </h2>
        <Progress
          className="info-block__progress-bar"
          transitionDuration={300}
          value={tasksToBeFinishedPercent}
          color={pinkColor}
        />
      </div>
    </div>
  )
}
