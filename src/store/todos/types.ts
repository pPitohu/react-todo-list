export enum Filter {
  all = 'all',
  completed = 'completed',
  notCompleted = 'not-completed'
}

export interface Task {
  isCompleted: boolean,
  content: string,
  id: string,
  isHidden: boolean
}

export interface TodosState {
  tasks: Task[],
  filter: Filter,
}
