import { PayloadAction, createSlice, nanoid } from '@reduxjs/toolkit'
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import { Filter, Task, TodosState } from './types'
import { createSelector } from 'reselect'

const persistConfig = {
  key: 'todos',
  storage
}

const initialState: TodosState = {
  tasks: [],
  filter: Filter.all,
}

const todosSlice = createSlice({
  name: 'todos',
  initialState,
  reducers: {
    setTasks: (state, action: PayloadAction<Task[]>) => {
      state.tasks = action.payload
    },
    addNewTask: (state, action: PayloadAction<string>) => {
      const shouldHideOnTaskCreation = state.filter === Filter.completed

      state.tasks.push({
        content: action.payload,
        isCompleted: false,
        id: nanoid(),
        isHidden: shouldHideOnTaskCreation
      })
    },
    removeTask: (state, action: PayloadAction<string>) => {
      state.tasks = state.tasks.filter(t => t.id !== action.payload)
    },
    renameTask: (state, action: PayloadAction<{
        id: string,
        newTaskContent: string
    }>
    ) => {
      const { id, newTaskContent } = action.payload
      const task = state.tasks.find(t => t.id === id)
      if (task) task.content = newTaskContent
    },
    toggleTaskCompleted: (state, action: PayloadAction<string>) => {
      const task = state.tasks.find(t => t.id === action.payload)
      if (task) task.isCompleted = !task.isCompleted
    },
    completeAllTasks: state => {
      state.tasks.map(t => t.isCompleted = true)
    },
    clearCompletedTasks: state => {
      state.tasks = state.tasks.filter(t => !t.isCompleted)
    },
    filterTasks: (state, action: PayloadAction<Filter>) => {
      state.filter = action.payload
      filterFunctions[state.filter](state)
    }
  }
})

const filterFunctions = {
  [Filter.all]: (state: TodosState) => state.tasks.map(t => t.isHidden = false),
  [Filter.notCompleted]: (state: TodosState) => state.tasks.map(t => t.isHidden = t.isCompleted),
  [Filter.completed]: (state: TodosState) => state.tasks.map(t => t.isHidden = !t.isCompleted),
}

export const completedTasksSelector = createSelector(
  state => state.tasks,
  (tasks: Task[]) => tasks.filter(t => t.isCompleted)
)

export const tasksToBeFinishedSelector = createSelector(
  state => state.tasks,
  (tasks: Task[]) => tasks.filter(t => !t.isCompleted)
)

export const {
  setTasks,
  addNewTask,
  removeTask,
  renameTask,
  toggleTaskCompleted,
  completeAllTasks,
  clearCompletedTasks,
  filterTasks
} = todosSlice.actions

export const persistedTodosReducer = persistReducer(persistConfig, todosSlice.reducer)
