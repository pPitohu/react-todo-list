import { configureStore } from '@reduxjs/toolkit'
import { persistedTodosReducer } from './todos'
import { persistStore } from 'redux-persist'
import { useDispatch, useSelector } from 'react-redux'

export const store = configureStore({
  reducer: persistedTodosReducer,
  middleware: getDefaultMiddleware => getDefaultMiddleware({
    serializableCheck: false,
  })
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

export const useAppDispatch = useDispatch.withTypes<AppDispatch>()
export const useAppSelector = useSelector.withTypes<RootState>()

export const persistor = persistStore(store)
