import Todos from './pages/Todos/Todos.tsx'
import './App.scss'

export default function App() {
  return (
    <main>
      <Todos />
    </main>
  )
}
