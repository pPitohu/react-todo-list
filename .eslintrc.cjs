module.exports = {
  root: true,
  env: { browser: true, es2020: true },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:react-hooks/recommended',
    'plugin:react/recommended',
    'plugin:react/jsx-runtime'
  ],
  ignorePatterns: ['dist', '.eslintrc.cjs'],
  parser: '@typescript-eslint/parser',
  plugins: ['react-refresh'],
  rules: {
    'react-refresh/only-export-components': [
      'warn',
      { allowConstantExport: true },
    ],
    'array-bracket-spacing': [ 'error', 'always', {
      'arraysInArrays': false,
      'objectsInArrays': false,
      'singleValue': false,
    }],
    'arrow-parens': [ 'error', 'as-needed' ],
    'eqeqeq': [ 'error', 'always' ],
    'eol-last': [ 'error', 'always' ],
    'indent': [ 'error', 2 ],
    'keyword-spacing': [ 'error', { 'after': true, 'before': true }],
    'max-len': [ 'error', {
      'code': 100,
      'ignoreComments': true,
      'ignorePattern': 'd="([\\s\\S]*?)"', // svg path property
      'ignoreRegExpLiterals': true,
      'ignoreStrings': true,
      'ignoreTemplateLiterals': true,
      'ignoreTrailingComments': true,
      'ignoreUrls': true,
      'tabWidth': 2,
    }],
    'no-console': 'off',
    'no-debugger': 'off',
    'no-multi-spaces': 'error',
    'no-multiple-empty-lines': [ 'error', { 'max': 1 }],
    'object-curly-spacing': [ 'error', 'always', {
      'arraysInObjects': false,
      'objectsInObjects': false,
    }],
    'prefer-const': 'off',
    'quotes': [ 'error', 'single' ],
    'semi': [ 'error', 'never' ],
    'space-before-blocks': 'error',
    'space-in-parens': [ 'error', 'never' ],
  },
}
